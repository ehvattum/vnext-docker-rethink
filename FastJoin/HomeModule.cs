namespace FastJoin
{
    using Nancy;
    using RethinkDb;
    using RethinkDb.Newtonsoft;
    using System.Net;
    using System.Threading.Tasks;
    using System.Collections.Generic;

    public class HomeModule : NancyModule
    {
        public HomeModule()
        {
            Get["/"] = _ => "Hello World";
            Get["tests",true] = (_,ct) => GetSomethingFromRethink();
        }

        private async Task<dynamic> GetSomethingFromRethink()
        {
            var conn = new Connection();
            conn.EndPoints = new EndPoint[]
            {
              new DnsEndPoint("rethink",28015)
            };
            await conn.ConnectAsync();
            var table = Query.Db("test").Table<Dictionary<string, object>>("tv_shows");
            var result =  conn.Run(table);
            return Response.AsJson(result);
        }
    }

}
